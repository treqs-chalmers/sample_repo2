# PlantUML code within markdown file

The following content is a sample class diagram

```puml
@startuml Class diagram for Meeting Scheduler
class Customer {
    Name
    Email
    Address
}
class MeetingInitiator {

}
class Participant{
}
note right of Participant
/'[requirement id=REQ0014 quality=QR0002 test=TC0001]'/
end note
class Meeting{
    location
    startTime
    endTime
}
class Room{
    roomNumber
}
User <|--Participant
User <|--MeetingInitiator
Meeting - "1..1" Room
Participant - " *..1" Meeting
MeetingInitiator - "1..*" Participant
@enduml
```

## Another embedded PlantUML code

```puml
@startuml

Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
note right of Alice
/'[requirement id=REQ0012 test=TC0005]'/

end note
@enduml
```

## Yet, another PlantUML code

```puml
@startuml
actor Foo1
boundary Foo2
control Foo3
entity Foo4
database Foo5
collections Foo6
Foo1 -> Foo2 : To boundary
Foo1 -> Foo3 : To control
Foo1 -> Foo4 : To entity
Foo1 -> Foo5 : To database
Foo1 -> Foo6 : To collections
note left of Foo1
/'[requirement id=REQ0013 quality=QR0002 test=TC0003]'/
end note
@enduml
```