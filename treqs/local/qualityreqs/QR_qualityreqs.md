# Meeting Scheduler System

Quality requirements for a fictitious meeting scheduler system

## [quality id=QR0001]

The elapsed time between the submission of a meeting request and the determination of the corresponding date or location should vary depending on the number of participants. If they are less than 20 people the elapsed time shall be less than 4 seconds, if the count is between 20 and 50, the elapsed time shall be less than 10 seconds. If the count is more than 50 the elapse time can vary from 10 seconds to 40 seconds.

## [quality id=QR0002]

Rescheduling of a meeting should be done dynamically.
